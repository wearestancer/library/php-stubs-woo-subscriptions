# Woo Subscriptions PHP Stubs

This package provides stub declarations for the [Woo Subscriptions plugin](https://woo.com/products/woocommerce-subscriptions/)
functions, classes and interfaces.
These stubs can help plugin and theme developers leverage IDE completion and static analysis tools like
[PHPStan](http://phpstan.org).

The stubs are generated directly from the source using [php-stubs/generator](https://github.com/php-stubs/generator).


## Installation

Require this package as a development dependency with [Composer](https://getcomposer.org).

```bash
composer require --dev stancer/php-stubs-woo-subscriptions
```


## Usage in PHPStan

Include stubs in PHPStan configuration file.

```yaml
parameters:
  scanFiles:
    - vendor/stancer/php-stubs-woo-subscriptions/stubs.php
```
